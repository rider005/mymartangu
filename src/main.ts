import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import firebase from 'firebase/app';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

try {
  // The default cache size threshold is 40 MB. Configure "cacheSizeBytes"
  // for a different threshold (minimum 1 MB) or set to "CACHE_SIZE_UNLIMITED"
  // to disable clean-up.
  firebase.firestore().settings({
    cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
  });

  firebase.firestore().enablePersistence().then(onfullFilled => {
    console.warn('Offline can work');
  }, error => {
    console.warn('' + error.message);
  });
} catch (err) {
  if (err.code === 'failed-precondition') {
    // Multiple tabs open, persistence can only be enabled
    // in one tab at a a time.
    // ...
    alert('Close other tab!');
  } else if (err.code === 'unimplemented') {
    // The current browser does not support all of the
    // features required to enable persistence
    // ...
    alert('The current browser does not support offline features!');
  }
}
// Subsequent queries will use persistence, if it was enabled successfully

if (environment.production) {
  enableProdMode();
}

// firebase.firestore().settings({
//   cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
// });

document.addEventListener('DOMContentLoaded', () => {
                                                                                                           platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
                                                                                                         });
