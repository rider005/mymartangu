import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/firestore';
import * as PaytmChecksum from 'PaytmChecksum';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  orderID = 'ORDERID_98797';
  /* initialize JSON String */
  // body = { mid: 'gSYTLX82547047335632', orderId: '01' };
  paytmParams = {
    head: {},
    body: {}
  };
  constructor() {

    const MID = 'gSYTLX82547047335632';
    const MERCHANT_KEY = 'c!Ji88tF1uc7WotU';
    const custUID = 'CUST_010';
    //this.initiateTransaction(this.orderID, MID, MERCHANT_KEY, custUID);
  }

  // transactionStatus(MID: string, MERCHANT_KEY: string): void {
  //   const paytmParams = {
  //     head: {},
  //     body: {}
  //   };

  //   /* body parameters */
  //   paytmParams.body = {
  //     /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
  //     mid: MID,

  //     /* Enter your order id which needs to be check status for */
  //     orderId: this.orderID,
  //   };

  //   PaytmChecksum.generateSignature(JSON.stringify(paytmParams.body), MERCHANT_KEY).then((checksum: any) => {
  //     /* head parameters */
  //     paytmParams.head = {

  //       /* put generated checksum value here */
  //       signature: checksum
  //     };

  //     /* prepare JSON string for request */
  //     const postData = JSON.stringify(paytmParams);

  //     const options = {

  //       /* for Staging */
  //       hostname: 'securegw-stage.paytm.in',

  //       /* for Production */
  //       // hostname: 'securegw.paytm.in',

  //       port: 443,
  //       path: '/v3/order/status',
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Content-Length': postData.length
  //       }
  //     };

  //     // Set up the request
  //     let response = '';
  //     const postReq = https.request(options, (postRes) => {
  //       postRes.on('data', (chunk) => {
  //         response += chunk;
  //       });

  //       postRes.on('end', () => {
  //         console.warn('Status Method');
  //         console.log('Response: ', response);
  //       });
  //     });

  //     // post the data
  //     postReq.write(postData);
  //     postReq.end();
  //   });
  // }

  // processTransaction(orderID: string, MID: string, TxnToken: string, MERCHANT_KEY: string): void {
  //   const paytmParams = {
  //     head: {},
  //     body: {}
  //   };

  //   console.warn('TxnToken |' + TxnToken);

  //   paytmParams.body = {
  //     requestType: 'NATIVE',
  //     mid: MID,
  //     orderId: orderID,
  //     paymentMode: 'CREDIT_CARD', // WALLET
  //     cardInfo: '|4111111111111111|111|122032',
  //     authMode: 'otp',
  //     // channelCode: 'SBI',
  //     // paymentFlow: 'NONE',
  //     // payerAccount: '',          // VPA, this is needed for a UPI transaction.
  //     // walletType: 'PAYTMPG'
  //   };

  //   console.warn('Timestamp | ' + firebase.firestore.FieldValue.serverTimestamp().toString());

  //   paytmParams.head = {
  //     txnToken: TxnToken,
  //     // requestTimestamp: firebase.default.firestore.FieldValue.serverTimestamp(),
  //     // channelId: 'WEB',
  //   };

  //   const postData = JSON.stringify(paytmParams);
  //   const options = {
  //     /* for Staging */
  //     hostname: 'securegw-stage.paytm.in',

  //     /* for Production */
  //     // hostname: 'securegw.paytm.in',

  //     port: 443,
  //     path: '/theia/api/v1/processTransaction?mid=' + MID + '&orderId=' + orderID + '',
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'Content-Length': postData.length
  //     }
  //   };

  //   console.warn('processTransaction method above response declration');

  //   let response = '';
  //   const postReq = https.request(options, (postRes) => {
  //     postRes.on('data', (chunk) => {
  //       response += chunk;
  //     });

  //     postRes.on('end', () => {
  //       console.log('Response: ', response);
  //       this.transactionStatus(MID, MERCHANT_KEY);
  //     });
  //   });

  //   postReq.write(postData);
  //   postReq.end();
  // }

  // initiateTransaction(orderID: string, MID: string, MERCHANT_KEY: string, custUID: string): void {

  //   let checksum: string;
  //   // callbackUrl: 'https://merchant.com/callback',
  //   // callbackUrl: 'https://securegw-stage.paytm.in/theia/paytmCallback',
  //   this.paytmParams.body = {
  //     requestType: 'Payment',
  //     mid: MID,
  //     websiteName: 'WEBSTAGING',
  //     orderId: orderID,
  //     callbackUrl: 'https://us-central1-mymart-910ff.cloudfunctions.net/practiceHTTP/orderID=' + orderID + '&paytmChecksum=' + checksum + '',
  //     txnAmount: {
  //       value: '1.00',
  //       currency: 'INR',
  //     },
  //     userInfo: {
  //       custId: custUID,
  //       // firstName: '',
  //       // lastName: '',
  //       // mobile: '',
  //       // email: ''
  //     },
  //     // goods: [{
  //     //   merchantGoodsId: '', // Unique id for the goods item (item no)
  //     //   merchantShippingId: '', // Merchant shipping id
  //     //   snapshotUrl: '', // Product Image URL
  //     //   description: '', // Description of product
  //     //   category: '', // Category of Product
  //     //   quantity: '', // Quantity ordered
  //     //   unit: '',                       // Unit of quantity (KG/Litre)
  //     //   price: {         // Price of product
  //     //     value: '1.00', // This parameter contains the amount to be charged to the customer and can have two places of decimal.
  //     //     // Example: 1.00
  //     //     currency: 'INR' // This parameter indicates the currency in which transaction amount is to be deducted.
  //     //     // Possible Values: INR
  //     //   }
  //     // }],

  //     // shippingInfo: {
  //     //   merchantShippingId: '',
  //     //   trackingNo: '',
  //     //   carrier: '',
  //     //   chargeAmount: '',
  //     //   countryName: '',
  //     //   stateName: '',
  //     //   cityName: '',
  //     //   address1: '',
  //     //   address2: '',
  //     //   firstName: '',
  //     //   lastName: '',
  //     //   mobileNo: '',
  //     //   zipCode: '',
  //     //   email: ''
  //     // },

  //     // extendInfo: {
  //     //   udf1: '',
  //     //   udf2: '',
  //     //   udf3: '',
  //     //   mercUnqRef: '',
  //     //   comments: ''
  //     // }
  //   };

  //   const paytmChecksum = PaytmChecksum.generateSignature(JSON.stringify(this.paytmParams.body), MERCHANT_KEY);
  //   paytmChecksum.then((result: string) => {
  //     console.log('generateSignature Returns: ' + result);
  //     checksum = result;
  //     this.paytmParams.head = {
  //       // channelId: 'WEB',
  //       // requestTimestamp: firebase.default.firestore.FieldValue.serverTimestamp(),
  //       signature: result
  //     };

  //     const postData = JSON.stringify(this.paytmParams);

  //     const options = {
  //       /* for Staging */
  //       hostname: 'securegw-stage.paytm.in',

  //       /* for Production */
  //       // hostname: 'securegw.paytm.in',

  //       port: 443,
  //       path: '/theia/api/v1/initiateTransaction?mid=' + MID + '&orderId=' + orderID + '',
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Content-Length': postData.length
  //       }
  //     };

  //     let response: string;
  //     const postReq = https.request(options, (postRes) => {
  //       postRes.on('data', (chunk) => {
  //         response += chunk;
  //       });

  //       postRes.on('end', () => {
  //         console.log('Response: ', response);
  //         response = response.substring(9);
  //         const res = JSON.parse(response);
  //         if (res.body.resultInfo.resultStatus === 'S') {
  //           console.warn(res.body.txnToken);
  //           this.processTransaction(orderID, MID, res.body.txnToken, MERCHANT_KEY);
  //         }
  //       });
  //     });

  //     postReq.write(postData);
  //     postReq.end();
  //   }).catch((error: any) => {
  //     console.log(error);
  //   });
  // }

  getOrderID(): string {
    return this.orderID;
  }
}
