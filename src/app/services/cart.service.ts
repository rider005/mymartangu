import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root',
})

export class CartService {
  // cartProductList: Array<object> = [];
  uId = '';
  constructor(private firestore: AngularFirestore, uId: string) {
    this.uId = uId;
    // this.loadCartList();
  }

  async loadCartList(): Promise<firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>> {
    // console.warn('all USEr' + localStorage.getItem('allUserCollName'));
    // console.warn(this.uId);
    // console.warn('product ' + localStorage.getItem('ProductCatCollName'));
    // console.warn('product list ' + localStorage.getItem('ProductListCollName'));     
    return (await this.firestore
      .collection(localStorage.getItem('allUserCollName') + '/' + this.uId +
        '/' + localStorage.getItem('userCartListCollName'))
      .ref.orderBy('time', 'asc')
      .get());
  }
}
