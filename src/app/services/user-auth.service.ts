import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

interface CurrentUser {
  uId: string;
}

@Injectable({
  providedIn: 'root'
})

export class UserAuthService implements CurrentUser {

  user: Observable<firebase.User>;
  uId: string;

  constructor(private firebaseAuth: AngularFireAuth) {
    this.user = firebaseAuth.authState;
    this.user.subscribe(user => {
      if (user) {
        this.uId = user.uid;
        this.userDetails({ uId: user.uid });
      }
    }, error => {
      console.warn(error.message);
    });
  }

  userDetails(user: CurrentUser): void {
    user.uId = this.uId;
    // console.warn(user.uId);
  }

  async sendVerificationEmail(user: firebase.User): Promise<void> {
    try {
      await user.sendEmailVerification();
      // Email sent.
      alert('Check you email inbox to verifiy it!!!');
    } catch (error) {
      // An error happened.
      console.warn('email for verify send error code' + error.code);
      console.warn('email for verify send error' + error.message);
    }
  }

  async logout(): Promise<void> {
    return await this.firebaseAuth.signOut();
  }
}
