import { TestBed } from '@angular/core/testing';

import { CartWishListService } from './cart-wish-list.service';

describe('CartWishListService', () => {
  let service: CartWishListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CartWishListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
