import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class CartWishListService {
  uId: string;

  constructor(private firestore: AngularFirestore, uId: string) {
    this.uId = uId;
  }

  async addItemToWishList(item: object) {
    const dataInWishList = await this.firestore
      .collection(localStorage.getItem('allUserCollName') + '/' + this.uId +
        '/' + localStorage.getItem('userWishListCollName')).add({ item })
    console.warn(dataInWishList);
  }

  async getWishList() {
    const wishList = (await this.firestore
      .collection(localStorage.getItem('allUserCollName') + '/' + this.uId +
        '/' + localStorage.getItem('userWishListCollName'))
      .ref
      .get()).docs
    // .ref.orderBy('time', 'asc')
    console.warn(wishList);

    return wishList;
  }
}
