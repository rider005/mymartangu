import { Observable } from 'rxjs/internal/Observable';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  catList: Observable<any[]>;
  constructor(firestore: AngularFirestore) {
    this.catList = firestore.collection('productCategories').valueChanges();
  }
  bannerImg =
    'https://firebasestorage.googleapis.com/v0/b/mymart-910ff.appspot.com/o/bigBanner.png?alt=media&token=4669d420-7800-445f-ab67-391b15b835bd';

  // SVG File
  b =
    'https://firebasestorage.googleapis.com/v0/b/mymart-910ff.appspot.com/o/bannerForbigScreen.svg?alt=media&token=54701251-1664-430f-9aaa-da21346ea713';
  getCatList(): Observable<any[]> {
    return this.catList;
  }

  ngOnInit(): void { }
}
