import { Observable } from 'rxjs/internal/Observable';
import { HomeComponent } from './../home/home.component';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  catList: Observable<any[]>;
  brandsList: Observable<any[]>;
  static _snackBar: MatSnackBar;

  constructor(firestore: AngularFirestore, _snackBar: MatSnackBar) {
    FooterComponent._snackBar = _snackBar
    this.catList = new HomeComponent(firestore).catList;

    this.brandsList = firestore
      .collection('brandsList', (ref) => ref.orderBy('name'))
      .valueChanges();
  }

  // catNameList = ['', 'Milk products', 'Beverages'];
  // brandsList = ['', '', '', '', '', ''];

  ngOnInit(): void { }
  // When the user clicks on the button, scroll to the top of the document
  topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  static openSnackBar(message: string, action: string): void {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
