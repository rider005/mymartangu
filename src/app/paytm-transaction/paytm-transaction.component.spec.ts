import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaytmTransactionComponent } from './paytm-transaction.component';

describe('PaytmTransactionComponent', () => {
  let component: PaytmTransactionComponent;
  let fixture: ComponentFixture<PaytmTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaytmTransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaytmTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
