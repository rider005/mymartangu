import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import firebase from 'firebase/app'
import { FooterComponent } from '../footer/footer.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirePerformance } from '@angular/fire/performance';
import { AngularFireAnalytics } from '@angular/fire/analytics';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSlideToggleChange } from '@angular/material/slide-toggle/slide-toggle';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  // cartProductList: Array<object>;
  catID: string;
  products: Array<object> = [];
  allUserCollName = 'allUsers';
  uId = '';
  isChecked = false;
  deletedObj = [];
  // flavourFormGroup: FormGroup;
  // flavours: any;
  // checked: boolean[];
  // cartProductList: Array<object> = [];
  // isCartListEmpty: boolean;

  constructor(
    private route: ActivatedRoute, private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer,
    private router: Router, private performance: AngularFirePerformance, private analytics: AngularFireAnalytics,
    private auth: AngularFireAuth, private firestore: AngularFirestore,
  ) {
    const colProductsList = 'productsList';
    this.analytics.setCurrentScreen('productsList');

    this.route.paramMap.subscribe(async paramMap => {
      this.deletedObj = [];
      this.catID = paramMap.get('catId');
      localStorage.setItem('catID', this.catID);
      this.products = [];
      performance.trace('loadProducts').then(loadProducts => {
        loadProducts.start();
        this.loadProducts(localStorage.getItem('ProductCatCollName'), colProductsList);

        // new ProductsService(this.firestore, this.performance)
        //   ?.loadProducts(colCatListName, colProductsList, this.catID).then(products => {
        //     this.products = products
        //     this.analytics.logEvent('loadProducts', {
        //       loadDone: true
        //     });
        //   }, (error) => {
        //     console.warn(error.message);
        //     this.analytics.logEvent('loadProducts', {
        //       loadDone: false,
        //       errorCode: error.code,
        //       errorMessage: error.message
        //     });
        //     loadProducts.putAttribute('errorCode', error.code);
        //     loadProducts.putAttribute('errorMessage', error.message);
        //   });

        loadProducts.stop();
      }).catch(error => {
        console.warn(error.message);
      });
    });
  }

  async ngOnInit(): Promise<void> {
    try {
      this.uId = (await this.auth.currentUser).uid;
    } catch (error) {
      console.log(error.message);
    }

    this.iconRegistry.addSvgIcon(
      'notLikedProduct',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/notLikedForWishList.svg'
      )
    );

    this.iconRegistry.addSvgIcon(
      'addToCart',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/addToCart.svg')
    );

    this.iconRegistry.addSvgIcon(
      'likedProduct',
      this.sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/likedForWishList.svg'
      )
    );
    // this.catID = this.route.snapshot.paramMap.get('catId');
    // localStorage.setItem('catID', this.catID);
    // this.loadProducts(colCatListName, colProductsList);

    // this.flavourFormGroup = this.formBuilder.group({
    //   flavours: this.formBuilder.array([])
    // });    
  }

  async onChange(event, flavourName: { key: any; value: any; }, productIndex: string, flavourIndex: string): Promise<void> {
    // console.log(event.checked);
    this.products[productIndex].checkedMessage = '';
    // const flavours = this.flavourFormGroup.get('flavours') as FormArray;
    if (event.checked) {
      // flavours.push(new FormControl(flavourName));
      await this.products[productIndex].selectedFlavours.splice(flavourIndex, 0, flavourName);
    } else {
      // const i = flavours.controls.findIndex(x => x.value === flavourName);
      // flavours.removeAt(i);
      await this.products[productIndex].selectedFlavours.splice(
        this.products[productIndex].selectedFlavours.indexOf(flavourName), 1);
    }
    // console.warn(flavours);
  }

  async loadProducts(colCatListName: string, colProductsList: string): Promise<void> {
    this.performance.trace('loadProducts').then(async loadProductsTrace => {
      loadProductsTrace.start();
      try {
        (await this.firestore.collection(colCatListName + '/' + this.catID + '/' + colProductsList).ref
          .get()).forEach(async (product: firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>) => {
            const data = product.data();
            if (data.flavours) {
              this.products.push(Object.assign(data, { id: product.id },
                { selectedFlavours: [], checkedMessage: '' }));
            } else {
              this.products.push(Object.assign(data, { id: product.id }));
            }
          });
        this.analytics.logEvent('loadProducts', {
          loadDone: true
        });
      } catch (error) {
        console.warn(error.message);
        loadProductsTrace.putAttribute('errorMessage', error.message);
        loadProductsTrace.putAttribute('errorCode', error.code);
        this.analytics.logEvent('loadProducts', {
          loadDone: false,
          errorCode: error.code,
          errorMessage: error.message
        });
      }

      loadProductsTrace.stop();
    }).catch(error => {
      console.warn(error.message);
    });
  }

  goToProductDetails(catID: string, ID: string): void {
    this.router.navigate(['/products', catID, 'productInfo', ID]);
  }

  async addToWishList(pId: string, productIndex: string) {
    if (this.uId) {
      this.firestore.collection(localStorage.getItem('allUserCollName') + '/' + this.uId +
        '/' + localStorage.getItem('userWishListCollName')).get().subscribe(dataa => {
          if (dataa && dataa.docChanges) {
            let count = 0;

            if (dataa.docs.length > 0) {
              dataa.docChanges().forEach((e => {
                const data: firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData> = e.doc;
                if (e.oldIndex === -1 && data.get('p_id') === pId) {
                  count++;
                  if (this.products[productIndex].selectedFlavours) {
                    this.products[productIndex].selectedFlavours.forEach(async (element: any) => {
                      if (data.data().selectedFlavours.indexOf(element) === -1) {
                        // online data don't have this element so add it
                        await this.firestore.doc(this.allUserCollName + '/'
                          + this.uId + '/userWishList/' + pId)
                          .update({
                            selectedFlavours: firebase.firestore.FieldValue
                              .arrayUnion(element)
                          });
                        this.analytics.logEvent('flavourAdded', {
                          name: element,
                        });
                        // await this.firestore.collection(this.allUserCollName)
                        //   .doc(this.uId).collection('userCartList').doc(pId)
                        //   .update({
                        //     selectedFlavours: firebase.firestore.FieldValue
                        //       .arrayUnion(element)
                        //   });
                        // console.log('Element added');
                        FooterComponent.openSnackBar('Flavour added', 'Ok');
                      }
                      if (data.data().selectedFlavours) {
                        data.data().selectedFlavours.forEach(async (element: any) => {
                          if (this.products[productIndex].selectedFlavours.indexOf(element) === -1) {
                            // offline data don't have this element so remove it from online data

                            await this.firestore.doc(this.allUserCollName + '/'
                              + this.uId + '/userWishList/' + pId).update({
                                selectedFlavours: firebase.firestore.FieldValue.arrayRemove(element)
                              });

                            this.analytics.logEvent('flavourRemoved', {
                              name: element,
                            });
                            // await this.firestore.collection(this.allUserCollName)
                            //   .doc(this.uId).collection('userCartList').doc(pId)
                            //   .update({
                            //     selectedFlavours: firebase.firestore.FieldValue.arrayRemove(element)
                            //   });
                            // console.log('Element removed');
                            FooterComponent.openSnackBar('Flavour removed', 'Ok');

                            if (this.products[productIndex].flavours) {
                              if (this.products[productIndex].selectedFlavours.length < 1) {
                                this.products[productIndex].checkedMessage = 'Select atleast one flavour';
                                await this.removeProductFromWishList(pId, productIndex)
                                return;
                              }
                            }
                          }
                        });
                      } else if (e.newIndex === -1) {
                        // console.warn('Removed from cart event');
                      }
                    });
                  }
                }
              }));
            } else {
              if (this.products[productIndex].flavours) {
                if (this.products[productIndex].selectedFlavours.length >= 1) {
                  this.addProductToWishList(this.catID, pId, productIndex);
                } else {
                  this.products[productIndex].checkedMessage = 'Select atleast one flavour';
                }
                return;
              }
              this.addProductToWishList(this.catID, pId, productIndex);
            }
          }
        }, error => {
          console.warn(error.message);
        })
    }
  }

  addToCart(pId: string, productIndex: string): void {
    // if (!this.uId) {
    //   console.warn('User is not signedIn');
    //   FooterComponent.openSnackBar('Do signIn', 'OK');
    // }
    if (this.uId) {
      // console.log('cId = ' + this.catID);
      // console.log('pId = ' + pId);

      this.firestore
        .collection(localStorage.getItem('allUserCollName'))
        .doc(this.uId)
        .collection(localStorage.getItem('userCartListCollName'))
        .get()
        .subscribe(async success => {
          if (success && success.docChanges) {
            let count = 0;
            if (success.docs.length > 0) {
              success.docChanges().forEach((e => {
                // this.isCartListEmpty = true;
                const data = e.doc;
                if (e.oldIndex === -1 && data.get('p_id') === pId) {
                  count++;
                  // console.log(data.data().selectedFlavours);
                  // console.log(this.products[productIndex].selectedFlavours);
                  if (this.products[productIndex].selectedFlavours) {
                    this.products[productIndex].selectedFlavours.forEach(async (element: any) => {
                      if (data.data().selectedFlavours.indexOf(element) === -1) {
                        // online data don't have this element so add it
                        await this.firestore.doc(this.allUserCollName + '/'
                          + this.uId + '/userCartList/' + pId)
                          .update({
                            selectedFlavours: firebase.firestore.FieldValue
                              .arrayUnion(element)
                          });
                        this.analytics.logEvent('flavourAdded', {
                          name: element,
                        });
                        // await this.firestore.collection(this.allUserCollName)
                        //   .doc(this.uId).collection('userCartList').doc(pId)
                        //   .update({
                        //     selectedFlavours: firebase.firestore.FieldValue
                        //       .arrayUnion(element)
                        //   });
                        // console.log('Element added');
                        FooterComponent.openSnackBar('Flavour added', 'Ok');
                      }
                    });
                  }
                  if (data.data().selectedFlavours) {
                    data.data().selectedFlavours.forEach(async (element: any) => {
                      if (this.products[productIndex].selectedFlavours.indexOf(element) === -1) {
                        // offline data don't have this element so remove it from online data

                        await this.firestore.doc(this.allUserCollName + '/'
                          + this.uId + '/userCartList/' + pId).update({
                            selectedFlavours: firebase.firestore.FieldValue.arrayRemove(element)
                          });

                        this.analytics.logEvent('flavourRemoved', {
                          name: element,
                        });
                        // await this.firestore.collection(this.allUserCollName)
                        //   .doc(this.uId).collection('userCartList').doc(pId)
                        //   .update({
                        //     selectedFlavours: firebase.firestore.FieldValue.arrayRemove(element)
                        //   });
                        // console.log('Element removed');
                        FooterComponent.openSnackBar('Flavour removed', 'Ok');

                        if (this.products[productIndex].flavours) {
                          if (this.products[productIndex].selectedFlavours.length < 1) {
                            this.products[productIndex].checkedMessage = 'Select atleast one flavour';
                            await this.removeProductFromCart(pId, productIndex);
                            return;
                          }
                        }
                      }
                    });
                  }
                } else if (e.newIndex === -1) {
                  // console.warn('Removed from cart event');
                }
              }));
              if (count === 0) {
                // console.warn('product is not in CART');
                if (this.products[productIndex].flavours) {
                  if (this.products[productIndex].selectedFlavours.length >= 1) {
                    this.addProductToCart(this.catID, pId, productIndex);
                  } else {
                    this.products[productIndex].checkedMessage = 'Select atleast one flavour';
                    return;
                  }
                } else {
                  this.addProductToCart(this.catID, pId, productIndex);
                }
              } else {
                // console.warn('product is already in cart');
                const successDelMessage = 'product is already in cart';
                FooterComponent.openSnackBar(successDelMessage, 'Ok');
              }
            } else {
              // this.isCartListEmpty = true;
              // console.warn('product is not in CART');
              if (this.products[productIndex].flavours) {
                if (this.products[productIndex].selectedFlavours.length >= 1) {
                  this.addProductToCart(this.catID, pId, productIndex);
                } else {
                  this.products[productIndex].checkedMessage = 'Select atleast one flavour';
                }
                return;
              }
              this.addProductToCart(this.catID, pId, productIndex);
            }
          }
        }, error => {
          console.warn(error.message);
        });
    }
  }

  async addProductToWishList(catID: string, pId: string, productIndex: string): Promise<void> {
    if (this.uId) {
      try {
        let obj: object;
        if (this.products[productIndex].selectedFlavours) {
          obj = {
            selectedFlavours: this.products[productIndex].selectedFlavours,
            c_id: catID,
            p_id: pId,
            time: firebase.firestore.FieldValue.serverTimestamp()
          };
        } else {
          obj = {
            c_id: catID,
            p_id: pId,
            time: firebase.firestore.FieldValue.serverTimestamp()
          };
        }
        await this.firestore.doc(this.allUserCollName + '/' + this.uId + '/userWishList/' + pId)
          .set(obj);
        // console.log('Product added to cart Successfully !');

        await this.firestore
          .doc(this.allUserCollName + '/' + this.uId)
          .update({
            userWish: firebase.firestore.FieldValue.increment(1)
          });
        this.analytics.logEvent('add_to_wish', {
          currency: 'INR',
          items: this.products[productIndex],
        });
        const successDelMessage = 'product added to wish list';
        FooterComponent.openSnackBar(successDelMessage, 'Ok');
      } catch (error) {
        console.warn(error);
        this.analytics.logEvent('add_to_wish', {
          errorMessage: error.message,
          errorCode: error.code
        });
      }
    }
  }

  async addProductToCart(catID: string, pId: string, productIndex: string): Promise<void> {
    if (this.uId) {
      try {
        let obj: object;
        if (this.products[productIndex].selectedFlavours) {
          obj = {
            selectedFlavours: this.products[productIndex].selectedFlavours,
            c_id: catID,
            p_id: pId,
            time: firebase.firestore.FieldValue.serverTimestamp()
          };
        } else {
          obj = {
            c_id: catID,
            p_id: pId,
            time: firebase.firestore.FieldValue.serverTimestamp()
          };
        }
        await this.firestore.doc(this.allUserCollName + '/' + this.uId + '/userCartList/' + pId)
          .set(obj);
        // console.log('Product added to cart Successfully !');

        await this.firestore
          .doc(this.allUserCollName + '/' + this.uId)
          .update({
            userCart: firebase.firestore.FieldValue.increment(1)
          });
        this.analytics.logEvent('add_to_cart', {
          currency: 'INR',
          items: this.products[productIndex],
        });
        const successDelMessage = 'product added to cart';
        FooterComponent.openSnackBar(successDelMessage, 'Ok');
      } catch (error) {
        console.warn(error);
        this.analytics.logEvent('add_to_cart', {
          errorMessage: error.message,
          errorCode: error.code
        });
      }
    }
  }

  async removeProductFromWishList(pId: string, productIndex: string) {
    if (this.uId) {
      try {
        await this.firestore.doc(localStorage.getItem('allUserCollName') + '/'
          + this.uId + localStorage.getItem('userWishListCollName') + pId).delete()

        await this.firestore.doc(localStorage.getItem('allUserCollName') + '/' + this.uId)
          .update({
            // userCart: data.get('userCart') - 1
            userWish: firebase.firestore.FieldValue.increment(-1)
          });

        this.analytics.logEvent('remove_from_wish', {
          items: this.products[productIndex]
        });
        FooterComponent.openSnackBar('Prodcut is removed', 'Ok');
      } catch (error) {
        console.warn(error.message);
        this.analytics.logEvent('remove_from_wish', {
          errorMessage: error.message,
          errorCode: error.code
        });
      }
    }
  }

  async removeProductFromCart(pId: string, productIndex: string): Promise<void> {
    if (this.uId) {
      try {
        await this.firestore.doc(localStorage.getItem('allUserCollName') + '/'
          + this.uId + localStorage.getItem('userCartListCollName') + pId).delete()

        // await this.firestore
        //   .collection(localStorage.getItem('allUserCollName'))
        //   .doc(this.uId)
        //   .collection(localStorage.getItem('userCartListCollName'))
        //   .doc(pId)
        //   .delete();

        // const data = await firebase.firestore().doc(localStorage.getItem('allUserCollName') + '/'
        //   + this.uId).get();
        await this.firestore.doc(localStorage.getItem('allUserCollName') + '/' + this.uId)
          .update({
            // userCart: data.get('userCart') - 1
            userCart: firebase.firestore.FieldValue.increment(-1)
          });

        this.analytics.logEvent('remove_from_cart', {
          items: this.products[productIndex]
        });
        FooterComponent.openSnackBar('Prodcut is removed', 'Ok');
      } catch (error) {
        console.warn(error.message);

        this.analytics.logEvent('remove_from_cart', {
          errorMessage: error.message,
          errorCode: error.code
        });
      }
    }
  }

  bhartiyaToggle(event: MatSlideToggleChange) {
    if (event.checked) {
      let index = 0;
      this.products.forEach(product => {
        const productDetails = JSON.parse(JSON.stringify(product));
        //console.warn(productDetails.isBhartiyaBrand);
        if (!productDetails.isBhartiyaBrand) {
          this.products.splice(index, 1);
          this.deletedObj.push(Object.assign(product, { index: index }));
        }
        index++;
      })
    } else {
      this.deletedObj.forEach(product => {
        const notBhartiyaProduct = JSON.parse(JSON.stringify(product));
        this.products.splice(notBhartiyaProduct.index, 0, product)
      });
      this.deletedObj = []
    }
  }
}
