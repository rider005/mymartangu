import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import * as $ from 'jquery';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { UserAuthService } from '../services/user-auth.service';
import { Observable } from 'rxjs/internal/Observable';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { AngularFirestore } from '@angular/fire/firestore';
import { FooterComponent } from '../footer/footer.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

export class HeaderComponent implements OnInit {

  // Size of browser viewport.
  // height = $(window).height();
  // width = $(window).width();
  // for firebase data
  items: Observable<any[]>;
  stateList: Observable<any[]>;
  userIsSignIn = false;

  // for product search autocomplate
  myControl = new FormControl();
  options: string[] = []; // Product Search List will be stored here
  filteredOptions: Observable<string[]>; //  Observe value of Product Search which is searched

  // for city search autocomplate
  cityMyControl = new FormControl();
  cityOptions: string[] = []; // City List will be stored here
  cityFilteredOptions: Observable<string[]>; //  Observe value of City List which is searched
  checkForVerifiedInterval;

  constructor(
    private firestore: AngularFirestore,
    auth: AngularFireAuth,
    private router: Router,
    private userAuth: UserAuthService,
    private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer
  ) {
    localStorage.setItem('allUserCollName', 'allUsers');
    localStorage.setItem('ProductCatCollName', 'productCategories');
    localStorage.setItem('ProductListCollName', 'productsList');
    localStorage.setItem('userCartListCollName', 'userCartList');
    localStorage.setItem('userWishListCollName', 'userWishList');
    localStorage.setItem('userOrderListCollName', 'userOrderList');
    localStorage.setItem('chargesForServicesCollName', 'chargesForServices');
    localStorage.setItem('userFeedBackCollName', 'userFeedbackList');
    localStorage.setItem('userProblemsListCollName', 'userProblemsList');
    this.getUserUID(auth);

    auth.onAuthStateChanged(
      async (user) => {
        if (user) {
          const userDocRef = firestore.collection('allUsers').doc(user.uid);
          userDocRef.valueChanges().subscribe((complate) => {
            if (complate) {
              const userData = JSON?.parse(JSON.stringify(complate));
              this.checkForVerifiedInterval = setInterval(() => {
                user.reload().then(async () => {
                  if (user.emailVerified && !userData.userEmailVerified) {
                    clearInterval(this.checkForVerifiedInterval);
                    try {
                      await userDocRef.update({ userEmailVerified: true, })
                      console.log('User email verified successfully updated!');
                      return;
                    } catch (error) {
                      console.log(error.message);
                      console.log('User email verified not updated!');
                    }
                  }
                });
              }, 1000);
            }
          });

          this.userIsSignIn = true;
          console.warn('user is signedIn');
          console.warn(user.email);
          console.warn('user.emailVerified ' + user.emailVerified);
        }
      },
      (e) => {
        console.warn('user EEE' + e);
        this.router.navigateByUrl('/');
      }
    );
    this.items = this.firestore.collection('offersList').valueChanges();
    this.stateList = this.firestore.collection('stateList').valueChanges();

    this.loadAndPushCatList();
    this.loadAndPushCityList();
  }
  async getUserUID(auth: { currentUser: any; }) {
    localStorage.setItem('userUid', (await auth.currentUser).uid);
  }

  async loadAndPushCityList() {
    try {
      (await this.firestore.collection('cityList').ref
        .get()).docs.forEach((city) => {
          this.cityOptions.push(city.get('cityName'));
        });
    } catch (error) {
      console.log(error.message);
    }
  }

  async loadAndPushCatList() {
    try {
      (await this.firestore
        .collection(localStorage.getItem('ProductCatCollName')).ref
        .get()).docs.forEach(cat => {
          this.options.push(cat.get('name'));
        });
    } catch (error) {
      console.warn('' + error.message);
    }
  }

  ngOnInit(): void {
    this.iconRegistry.addSvgIcon(
      'side-nav',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/sideNav.svg'));

    this.iconRegistry.addSvgIcon(
      'wishList',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/wishList.svg'));

    this.iconRegistry.addSvgIcon(
      'notification',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/notifications.svg'));

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map((value: string) => this._filter(value))
    );

    this.cityFilteredOptions = this.cityMyControl.valueChanges.pipe(
      startWith(''),
      map((value: string) => this._cityFilter(value))
    );
  }

  async logOutUser(): Promise<void> {
    try {
      (await this.userAuth.logout())
      this.userIsSignIn = false;
      console.warn('loggedOut Done');
      this.router.navigateByUrl('/');
    } catch (error) {
      console.warn('loggedOut Has Errors' + error.message);
    }
  }
  // tslint:disable-next-line: ban-types
  displayFn(cityOptions: String): String {
    return cityOptions;
  }

  private _cityFilter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.cityOptions.filter(
      (option) => option.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    //console.log(filterValue);

    if (filterValue) {
      // let temp: string[] = []
      this.firestore
        .collection(localStorage.getItem('ProductCatCollName')).ref
        .get().then(docs => {
          docs.forEach(catList => {
            this.firestore.doc(catList.ref).collection(localStorage.getItem('ProductListCollName'))
              .ref.where('name', '>=', filterValue)
              .get().then(productsDocs => {
                productsDocs.forEach(prodcut => {
                  //console.log(prodcut.data());
                  //console.log(prodcut);
                  if (this.options.indexOf(prodcut.get('name')) === -1) {
                    this.options.push(prodcut.get('name'));
                  }
                  // temp.push(prodcut.get('name'));
                });
                // temp.forEach(tem => {
                //   this.options.push(tem);
                // });
              }).catch(error => {
                console.warn('' + error.message);
              })
            //this.options.push(catList.get('name'));
          });
        }, error => {
          console.warn('' + error.message);
        }).catch(error => {
          console.warn('' + error.message);
        });
    }
    //  else {
    //   console.warn('ELSE');

    // }
    return this.options.filter(
      (option) => option.toLowerCase().indexOf(filterValue) === 0
    );
  }

  searchSelectedProduct(event: MatAutocompleteSelectedEvent): void {
    this.hideKeyboard();
    const filterValue = event.option.value;
    // localStorage.setItem('catName', filterValue);
    if (filterValue) {
      this.firestore
        .collection(localStorage.getItem('ProductCatCollName'))
        .ref.where('name', '==', filterValue)
        .get()
        .then((success) => {
          success.forEach((detail) => {
            this.router.navigate(['/products', detail.id]);
          });
        },
          (error) => {
            console.warn('problem is ' + error.message);
            FooterComponent.openSnackBar('error.message', 'OK');
          }
        );
    }
  }

  private hideKeyboard(): void {
    document.getElementById('searchProduct').blur();
  }
}