import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { UserAuthService } from '../services/user-auth.service';
import firebase from 'firebase/app';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  // userdata: Observable<any>;
  name = '';
  email = '';
  isEmailVerified = false;
  emailVerifiedMessage = '';
  phone = '';

  city = '';
  state = '';
  address = '';

  pendingOrders = '';
  complatedOrders = '';

  wish = '';
  cart = '';
  user: firebase.User;

  constructor(
    public auth: AngularFireAuth,
    public userAuth: UserAuthService,
    firestore: AngularFirestore,
    private router: Router,
  ) {
    auth.authState.subscribe((user) => {
      // console.warn(authState.uid);
      if (user) {
        this.user = user;
        const userDocRef = firestore.collection('allUsers').doc(user.uid);

        userDocRef.valueChanges().subscribe(
          (complate) => {
            if (complate) {
              const userData = JSON?.parse(JSON.stringify(complate));

              this.name =
                userData.userName !== 'null' ? userData.userName : 'Name';
              this.email = userData.userEmail ?? 'Email';
              this.isEmailVerified = userData.userEmailVerified;
              this.emailVerifiedMessage = this.isEmailVerified
                ? 'Email is verified'
                : 'Click on verifiy emali to verify it!!!';
              this.pendingOrders =
                userData.userPendingOrders ?? 'Pending Orders';
              this.complatedOrders =
                userData.userCompletedOrders ?? 'Completed Orders';
              this.wish = userData.userWish ?? 'Wish';
              this.cart = userData.userCart ?? 'Cart';

              this.phone =
                userData.userPhone !== 'null'
                  ? userData.userPhone
                  : 'Phone Number';
              this.city =
                userData.userCity !== 'null' ? userData.userCity : 'City';
              this.state =
                userData.userState !== 'null' ? userData.userState : 'State';
              this.address =
                userData.userAddress !== 'null' ? userData.userAddress : 'Address';
            }
          },
          (error) => {
            console.warn(error);
            this.router.navigateByUrl('/');
          }
        );
      }
    });
  }

  updateProfile(): void {
    this.router.navigate(['/updateProfile', this.user.uid]);
  }

  sendVerificationEmail(): void {
    if (this.user) {
      this.userAuth.sendVerificationEmail(this.user);
    }
  }

  ngOnInit(): void { }
}
