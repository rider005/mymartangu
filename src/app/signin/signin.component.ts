import { AngularFireAuth } from '@angular/fire/auth';
import { UserAuthService } from './../services/user-auth.service';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import firebase from 'firebase/app';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirePerformance } from '@angular/fire/performance';
import { FooterComponent } from '../footer/footer.component';
import { AngularFireAnalytics } from '@angular/fire/analytics';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})

export class SigninComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,
    private router: Router, firestore: AngularFirestore, public userAuth: UserAuthService,
    private auth: AngularFireAuth, private performance: AngularFirePerformance,
    private analytics: AngularFireAnalytics
  ) {
    this.db = firestore;
    iconRegistry.addSvgIcon(
      'google-icon',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/google_icon.svg')
    );
    this.analytics.setCurrentScreen('SignIn');
  }
  screenTrace: firebase.performance.Trace;
  db: AngularFirestore;

  signInFormG: FormGroup;
  userPassword: AbstractControl;
  userEmail: AbstractControl;
  userName: AbstractControl;
  emailMessage: string;
  userNameMessage: string;
  passMessage: string;

  passVisHide = true; // For Password
  showNameEle = false;
  showPassEle = false;
  isPasswordInvalid = false;
  sendingPasswordResetLink = false;
  signInSignUp = false;
  allUserCollName = 'allUsers';

  async saveUserData(user: firebase.User): Promise<void> {
    const saveUserSignUpDataTrace = await this.performance.trace('saveUserSignUpData');
    saveUserSignUpDataTrace.start();
    try {
      const userData = {
        userEmail: user.email,
        userPhone: 'null',
        userPhoneVerified: false,
        userGender: 'null',
        userCity: 'null',
        userState: 'null',
        userAddress: 'null',
        userName: user.displayName ? user.displayName : 'null',
        userEmailVerified: user.emailVerified,
        userCompletedOrders: 0,
        userPendingOrders: 0,
        userCart: 0,
        userWish: 0,
        time: firebase.firestore.FieldValue.serverTimestamp(),
      };
      await this.db
        .collection(this.allUserCollName)
        .doc(user.uid)
        .set(userData);
      console.log('User Data Saved Successfully !');
      if (!user.emailVerified) {
        await this.userAuth.sendVerificationEmail(user);
        this.router.navigate(['']);
      }
    } catch (error) {
      console.error('Error While Saving Data: ', error);
      alert('' + error.message);
      saveUserSignUpDataTrace.putAttribute('errorMessage', error.message);
      saveUserSignUpDataTrace.putAttribute('errorCode', error.code);
    }
    saveUserSignUpDataTrace.stop();
    return;

    this.db
      .collection(this.allUserCollName)
      .doc(user.uid)
      .set({
        userEmail: user.email,
        userPhone: 'null',
        userPhoneVerified: false,
        userGender: 'null',
        userCity: 'null',
        userState: 'null',
        userAddress: 'null',
        userName: user.displayName ? user.displayName : 'null',
        userEmailVerified: user.emailVerified,
        userCompletedOrders: 0,
        userPendingOrders: 0,
        userCart: 0,
        userWish: 0,
      });
  }

  async onEmailPasswordFormSubmit(): Promise<void> {
    if (this.signInFormG.controls.userEmail.valid && this.userEmail.valid) {
      const userName = this.signInFormG.controls.userName.value;
      const email = this.signInFormG.controls.userEmail.value;
      let password = this.signInFormG.controls.userPassword.value;
      const demoPass = 'demoPassword';
      password = password ? password : demoPass;

      if (
        (password !== demoPass &&
          this.signInFormG.controls.userEmail.valid &&
          this.signInFormG.controls.userPassword.valid) ||
        !this.showPassEle
      ) {
        console.warn('email = ' + email);
        // console.warn('pass = ' + password);
        // console.warn('type of email = ' + typeof email);
        const userLoginTrace = await this.performance.trace('userLogin');
        userLoginTrace.start();
        try {
          this.signInSignUp = true;
          const user = (await this.auth.signInWithEmailAndPassword(email, password)).user;
          userLoginTrace.putAttribute('verified', String(user.emailVerified));
          userLoginTrace.stop();

          this.signInSignUp = false;
          console.warn('SignIn User Success using : ' + user.email);
          this.analytics.logEvent('login', this.onEmailPasswordFormSubmit());
          this.router.navigate(['']);
        } catch (error) {
          if (!error.message.includes('no user record')) {
            userLoginTrace.putAttribute('errorMessage', error.message);
            userLoginTrace.putAttribute('errorCode', error.code);
          }
          userLoginTrace.stop();

          this.signInSignUp = false;
          this.showPassEle = true;
          this.isPasswordInvalid = true;
          console.log(error.message);
          if (this.isPasswordInvalid && this.signInSignUp) {
            alert(error.message);
          }
          if (error.message.includes('no user record')) {
            // this.message = error.message;
            this.showNameEle = true;
            if (this.signInFormG.controls.userPassword.valid &&
              this.signInFormG.controls.userName.valid) {
              // this.userAuth.signUpErrorMessage = '';
              this.showNameEle = true;
              console.warn('SignUp User');

              const userSignUpTrace = await this.performance.trace('userSignUp');
              userSignUpTrace.start();

              try {
                this.signInSignUp = true;
                const user = (await this.auth.createUserWithEmailAndPassword(email, password)).user;
                console.warn('SignUp Success');
                this.analytics.logEvent('signUp With Email & Password');

                await user.updateProfile({ displayName: userName });
                console.warn('Name Updated');

                await this.saveUserData(user);
                this.signInSignUp = false;
              } catch (error) {
                this.signInSignUp = false;
                userSignUpTrace.putAttribute('errorMessage', error.message);
                userSignUpTrace.putAttribute('errorCode', error.code);
                console.warn('SignUp Catch', error.message);
              }
              userSignUpTrace.stop();
            }
          }
        }
      }
    }
  }

  togglePass(): void {
    this.passVisHide = !this.passVisHide;
  }

  async ngOnInit(): Promise<void> {
    this.signInFormG = this.formBuilder.group({
      userName: new FormControl('', [Validators.required,
      Validators.pattern(
        '([a-zA-Z\',.-]+( [a-zA-Z\',.-]+)*){3,30}'
      ),]),
      userEmail: new FormControl('', [Validators.required, Validators.email]),
      userPassword: new FormControl('', [
        Validators.required,
        Validators.pattern(
          '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{8,}'
        ),
      ]),
    });
    this.userName = this.signInFormG.get('userName');
    this.userEmail = this.signInFormG.get('userEmail');
    this.userPassword = this.signInFormG.get('userPassword');
    this.onChanges();
    this.screenTrace = await this.performance.trace('loginPage');
    this.screenTrace.start();
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy(): void {
    this.screenTrace.stop();
  }

  onChanges(): void {
    this.signInFormG.valueChanges.subscribe((val) => {
      // tslint:disable-next-line: triple-equals
      // console.warn(val.userPassword);

      if (this.userPassword.valid && this.userEmail.valid && this.userName.valid) {
        this.userNameMessage = '';
        this.emailMessage = '';
        this.passMessage = '';
      } else {
        if (val.userEmail.length === null) {
          this.emailMessage = 'Must Enter Email';
        } else if (!this.userEmail.valid) {
          this.emailMessage = 'Enter Valid Email';
          if (!val.userEmail.includes('@')) {
            this.emailMessage = this.emailMessage + ' Add @ in email id';
          }
          if (
            !val.userEmail.includes('.co') ||
            !val.userEmail.includes('.com')
          ) {
            this.emailMessage =
              this.emailMessage +
              ' Add .co OR .com in email id which applicable to you';
          }
        }

        if (val.userPassword.length === null) {
          this.passMessage = 'Must Enter Password';
        } else if (val.userPassword.length <= 8) {
          this.passMessage =
            'Enter minimum 9 letters with number, capital, special symbol, small letter';
        } else if (!this.userEmail.valid) {
          this.passMessage = 'Enter valid password';
        }

        if (val.userName.length === null) {
          this.userNameMessage = 'Must enter name Surname';
        } else if (val.userName.length > 3 && val.userName.length > 7) {
          this.userNameMessage =
            'Enter Your Name Surname';
        } else if (val.userName.length <= 3) {
          this.userNameMessage =
            'Enter minimum 3 letters';
        } else if (!this.userName.valid) {
          this.userNameMessage = 'Enter valid name';
        }
      }
    });
  }

  async signInWithGoogle(): Promise<void> {
    const signInWithGoogleTrace = await this.performance.trace('signInWithGoogle');
    signInWithGoogleTrace.start();
    const provider = new firebase.auth.GoogleAuthProvider();

    try {
      const user = (await this.auth.signInWithPopup(provider)).user;
      if (user.metadata.creationTime === user.metadata.lastSignInTime) {
        console.log('New User');
        this.analytics.logEvent('signInWithGoogle', {
          userType: 'New User'
        });
        await this.saveUserData(user);
      } else {
        this.analytics.logEvent('signInWithGoogle', {
          userType: 'OLD User'
        });
        console.log('OLD User');
      }
      signInWithGoogleTrace.stop();
      this.router.navigateByUrl('/');
    } catch (error) {
      console.log(error.message);
      signInWithGoogleTrace.putAttribute('errorMessage', error.message);
      signInWithGoogleTrace.putAttribute('errorCode', error.code);
      FooterComponent.openSnackBar(error.message, 'OK');
      signInWithGoogleTrace.stop();
    }
    return;

    // this.auth
    //   .signInWithPopup(provider)
    //   // tslint:disable-next-line: typedef
    //   .then((result) => {
    //     // This gives you a Google Access Token. You can use it to access the Google API.
    //     // The signed-in user info.
    //     const user = result.user;
    //   })
    //   .catch((error) => {
    //     console.log(error.message);
    //     // Handle Errors here.
    //     // The email of the user's account used.
    //     // The firebase.auth.AuthCredential type that was used.
    //     // ...
    //   });
  }

  async sendPasswordResetLink(): Promise<void> {
    console.warn('sendPasswordResetLink');
    if (this.signInFormG.controls.userEmail.valid) {
      this.sendingPasswordResetLink = true;
      const email = this.signInFormG.controls.userEmail.value;
      try {
        await this.auth.sendPasswordResetEmail(email);
        this.sendingPasswordResetLink = false;
        alert('Password reset link is sent to : ' + email);
      } catch (error) {
        this.sendingPasswordResetLink = false;
        if (error.message.includes('no user record')) {
          this.onEmailPasswordFormSubmit();
        }
        console.warn(error.message);
      }
    } else {
      console.warn('Enter valid email');
    }
  }
}
