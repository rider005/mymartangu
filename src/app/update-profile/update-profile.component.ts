import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import firebase from 'firebase/app';
import { FooterComponent } from '../footer/footer.component';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent implements OnInit {
  updateProfileFormG: FormGroup;

  userName: AbstractControl;
  userAddress: AbstractControl;
  userCity: AbstractControl;
  userState: AbstractControl;

  userNameMessage: string;
  userAddressMessage: string = '';
  userCityMessage: string = '';
  userStateMessage: string = '';

  user: firebase.User;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AngularFireAuth,
    private firestore: AngularFirestore,
    private router: Router) {
  }

  async ngOnInit(): Promise<void> {
    this.updateProfileFormG = this.formBuilder.group({
      userName: new FormControl('', [Validators.required, Validators.pattern(
        '([a-zA-Z\',.-]+( [a-zA-Z\',.-]+)*){3,30}'
      ),]),
      userAddress: new FormControl('', [Validators.required, Validators.minLength(10)]),
      userCity: new FormControl('', [Validators.required, Validators.minLength(3)]),
      userState: new FormControl('', [Validators.required, Validators.minLength(3)]),
    });
    this.userName = this.updateProfileFormG.get('userName');
    this.userAddress = this.updateProfileFormG.get('userAddress');
    this.userCity = this.updateProfileFormG.get('userCity');
    this.userState = this.updateProfileFormG.get('userState');

    try {
      this.user = await this.auth.currentUser;
      const userData = await this.firestore.doc('allUsers/' + this.user.uid).ref.get();

      this.userName.setValue(userData.get('userName') !== 'null' ? userData.get('userName') : '');
      this.userAddress.setValue(userData.get('userAddress') !== 'null' ? userData.get('userAddress') : '');
      this.userCity.setValue(userData.get('userCity') !== 'null' ? userData.get('userCity') : '');
      this.userState.setValue(userData.get('userState') !== 'null' ? userData.get('userState') : '');
    } catch (error) {
      console.warn(error.message);
    }

    this.onChanges();
  }
  onChanges() {
    this.updateProfileFormG.valueChanges.subscribe(async (val) => {
      if (val.userName.length === null) {
        this.userNameMessage = 'Must Enter Name';
      } else if (val.userName.length <= 3) {
        this.userNameMessage = 'Enter Atleast 3 letters';
      } else if (!this.userName.valid) {
        this.userNameMessage = 'Enter Valid Name';
      }

      if (val.userAddress.length === null) {
        this.userAddressMessage = 'Must Enter Address';
      } else if (val.userAddress.length < 10) {
        this.userAddressMessage = 'Enter atleast 10 letters';
      } else if (!this.userAddress.valid) {
        this.userAddressMessage = 'Enter Valid Address';
      }

      if (val.userCity.length === null) {
        this.userCityMessage = 'Must enter city';
      } else if (val.userCity.length < 3) {
        this.userCityMessage = 'Enter atleast 3 letters';
      } else if (!this.userCity.valid) {
        this.userCityMessage = 'Enter valid city';
      }

      if (val.userState.length === null) {
        this.userStateMessage = 'Must enter state';
      } else if (val.userState.length < 3) {
        this.userStateMessage = 'Must atleast 3 letters';
      } else if (!this.userState.valid) {
        this.userStateMessage = 'Enter valid state';
      }
    });
  }

  async onProfileUpdateFormSubmit(): Promise<void> {
    if (this.userName.valid && this.userAddress.valid
      && this.userCity.valid && this.userState.valid) {
      try {
        await this.firestore.doc('allUsers/' + this.user.uid).update({
          userName: this.userName.value,
          userAddress: this.userAddress.value,
          userCity: this.userCity.value,
          userState: this.userState.value,
        });
        FooterComponent.openSnackBar('User Profile Updated Succesfully!!!', 'OK');
        this.router.navigate(['/profile']);
      } catch (error) {
        console.warn(error.message);
      }
    }
  }
}
