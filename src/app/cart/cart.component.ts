import { CartService } from './../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import firebase from 'firebase/app';
import { FooterComponent } from '../footer/footer.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirePerformance } from '@angular/fire/performance';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})

export class CartComponent implements OnInit {
  isCartListEmpty: boolean;
  uId: string;
  cartProductList: Array<object> = [];
  totalPrice = 0;
  body: any;
  productOrderDeliveryCharge = 0;
  productsOrderGrandTotal = 0;

  MID = 'gSYTLX82547047335632';
  MERCHANT_KEY = 'c!Ji88tF1uc7WotU';
  orderID = 'ORDERID_98712';
  custUID = 'CUST_010';
  amount = '1.0';
  token = '';

  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private firestore: AngularFirestore,
    private auth: AngularFireAuth, private performance: AngularFirePerformance) {
  }

  async ngOnInit(): Promise<void> {
    try {
      this.uId = (await this.auth.currentUser).uid;
      this.iconRegistry.addSvgIcon(
        'notLikedProduct',
        this.sanitizer.bypassSecurityTrustResourceUrl(
          'assets/icons/notLikedForWishList.svg'
        )
      );
      await this.loadDeliveryCharge();
      await this.loadCartList(this.firestore);
    } catch (error) {
      console.log(error.message);
    }
  }

  async loadCartList(firestore: AngularFirestore): Promise<void> {
    const loadCartListTrace = await this.performance.trace('loadCartList');
    loadCartListTrace.start();
    try {
      this.cartProductList = [];
      const cartList = await new CartService(firestore, this.uId).loadCartList();
      if (cartList.docs.length > 0) {
        cartList.forEach(async doc => {
          this.isCartListEmpty = false;
          const docData = doc;
          const getIds = await this.firestore
            .collection(localStorage.getItem('ProductCatCollName'))
            .doc(docData.get('c_id'))
            .collection(localStorage.getItem('ProductListCollName'))
            .doc(docData.get('p_id')).ref
            .get();
          if (getIds.data() && getIds.exists) {
            if (!this.cartProductList.includes(getIds.data())) {
              // console.warn(getIds.data().flavours);
              if (docData.get('selectedFlavours') && getIds.data().flavours) {
                // console.warn(docData.get('selectedFlavours'));
                const updatedPrice = Number(getIds.get('price'))
                  * docData.get('selectedFlavours').length;
                const tempArray: Array<object> = [];
                getIds.data().flavours.forEach((flavour: any) => {
                  if (docData.get('selectedFlavours').indexOf(flavour) !== -1) {
                    tempArray.push(
                      {
                        name: flavour,
                        selected: true
                      }
                    );
                  } else {
                    tempArray.push(
                      {
                        name: flavour,
                        selected: false
                      }
                    );
                  }
                });
                // { selectedFlavours: docData.get('selectedFlavours') },
                this.cartProductList.push(
                  Object.assign(getIds.data(),
                    { id: docData.id },
                    { c_id: docData.get('c_id') },
                    { p_id: docData.get('p_id') },
                    { selectedFlavours: tempArray },
                    { total: updatedPrice }),
                );
                this.totalPrice = Number(this.totalPrice) + Number(getIds.get('price')
                  * docData.get('selectedFlavours').length);
              } else {
                this.totalPrice = Number(this.totalPrice) + Number(getIds.get('price'));
                this.cartProductList.push(
                  Object.assign(getIds.data(),
                    { id: docData.id },
                    { c_id: docData.get('c_id') },
                    { p_id: docData.get('p_id') })
                );
              }
              this.productsOrderGrandTotal = this.totalPrice + this.productOrderDeliveryCharge;
            }
          }
        });
      }
      else {
        this.isCartListEmpty = true;
      }
    } catch (error) {
      console.warn(error.message);
      loadCartListTrace.putAttribute('errorMessage', error.message);
      loadCartListTrace.putAttribute('errorCode', error.code);
    }
    loadCartListTrace.stop();
  }

  async onChange(e: { checked: any; }, flavourName: string, productIndex: string, flavourIndex: string): Promise<void> {
    // console.log(e.checked);
    // console.log(this.cartProductList[productIndex].selectedFlavours[flavourIndex].selected);

    // .selectedFlavours[flavourIndex].name
    // .selectedFlavours[flavourIndex].selected
    if (e.checked) {
      // await this.cartProductList[productIndex].selectedFlavours.splice(flavourIndex, 0, flavourName);
      this.cartProductList[productIndex].selectedFlavours[flavourIndex].selected = true;
      // this.products[productIndex].selectedFlavours[flavourIndex] = true
    } else {
      // await this.cartProductList[productIndex].selectedFlavours.splice(
      //   this.cartProductList[productIndex].selectedFlavours.indexOf(flavourName), 1);
      this.cartProductList[productIndex].selectedFlavours[flavourIndex].selected = false;

      // this.products[productIndex].selectedFlavours[flavourIndex] = false
    }
    // console.log(this.cartProductList[productIndex]);
  }

  async loadDeliveryCharge(): Promise<void> {
    try {
      const chargesDocData: firebase.firestore.DocumentData
        = await this.firestore
          .collection(localStorage.getItem('chargesForServicesCollName'))
          .doc('chargesList').ref.get();

      this.productOrderDeliveryCharge = Number(chargesDocData.data().deliveryCharge);
    }
    catch (error) {
      console.warn(error.message);
    }
  }

  async removeItemFromCart(index: number, docId: string, price: string, total: string): Promise<void> {

    await this.firestore
      .collection(localStorage.getItem('allUserCollName'))
      .doc(this.uId)
      .collection(localStorage.getItem('userCartListCollName'))
      .doc(docId)
      .delete();

    const data = await this.firestore.collection('allUsers').doc(this.uId).ref.get();
    if (data.exists && data.data()) {
      await this.firestore.collection('allUsers').doc(this.uId).update({
        userCart: data.get('userCart') - 1
      });
      if (total) {
        this.totalPrice = Number(this.totalPrice) - Number(total);
        this.productsOrderGrandTotal = Number(this.productsOrderGrandTotal) - Number(total);
      } else {
        this.totalPrice = Number(this.totalPrice) - Number(price);
        this.productsOrderGrandTotal = Number(this.productsOrderGrandTotal) - Number(price);
      }
      // console.log('totalPrice : ' + this.totalPrice);

      // console.warn('decrement of size as product is deleted');

      FooterComponent.openSnackBar('Prodcut is removed', 'Ok');

      this.cartProductList.splice(index, 1);
      if (this.cartProductList.length === 0) {
        this.isCartListEmpty = true;
        // tslint:disable-next-line: no-shadowed-variable
        const successDelMessage = 'Cartlist is empty now';
        FooterComponent.openSnackBar(successDelMessage, 'Ok');
      }
    }
    return;

    this.firestore
      .collection(localStorage.getItem('allUserCollName'))
      .doc(this.uId)
      .collection(localStorage.getItem('userCartListCollName'))
      .doc(docId)
      .delete()
      .then(
        async () => {
          this.firestore.collection('allUsers').doc(this.uId).get().subscribe(async data => {
            if (data.exists && data.data()) {
              this.totalPrice = this.totalPrice - Number(price);
              await this.firestore.collection('allUsers').doc(this.uId).update({
                userCart: data.get('userCart') - 1
              });
              console.warn('decrement of size as product is deleted');
            }
          }, error => {
            console.warn(error.message);
          });

          const successDelMessage = 'Prodcut is removed';
          FooterComponent.openSnackBar(successDelMessage, 'Ok');
          console.warn(successDelMessage);

          this.cartProductList.splice(index, 1);
          if (this.cartProductList.length === 0) {
            this.isCartListEmpty = true;
            // tslint:disable-next-line: no-shadowed-variable
            const successDelMessage = 'Cartlist is empty now';
            FooterComponent.openSnackBar(successDelMessage, 'Ok');
          }
        },
        // tslint:disable-next-line: no-shadowed-variable
        (error) => {
          console.warn(error.message);
        }
      );
  }

  async Pay(): Promise<void> {
    const myOrderList: { c_id: string, p_id: string, price: string }[] = [];
    this.cartProductList.forEach(cartProduct => {
      const catProductData = JSON?.parse(JSON.stringify(cartProduct));
      myOrderList.push(Object.assign(
        { c_id: catProductData.c_id },
        { p_id: catProductData.p_id },
        { price: catProductData.price }
      ));
    });

    try {
      await this.firestore
        .collection(localStorage.getItem('allUserCollName'))
        .doc(this.uId)
        .collection(localStorage.getItem('userOrderListCollName'))
        .doc()
        .set({
          orderList: myOrderList,
          orderGrandTotal: String(this.productsOrderGrandTotal),
          orderDeliveryCharge: String(this.productOrderDeliveryCharge),
          orderTotal: String(this.totalPrice),
          time: firebase.firestore.FieldValue.serverTimestamp()
        });
      console.log('Your order is placed');
      setTimeout(() => {
        this.loadCartList(this.firestore);
      }, 10000);
    } catch (error) {
      console.log('Sorry we could \'t place the order');
      console.log('' + error.message);
    }

    // .doc(docData.get('p_id'))
    // const d = 
    //new PaymentService();
    // this.initiateTransaction(this.orderID, this.MID, this.MERCHANT_KEY, this.custUID);
  }

  // initiateTransaction(orderID: string, MID: string, MERCHANT_KEY: string, custUID: string): void {

  //   const paytmParams = {
  //     head: {},
  //     body: {}
  //   };

  //   let checksum: string;
  //   // callbackUrl: 'https://merchant.com/callback',
  //   // callbackUrl: 'https://securegw-stage.paytm.in/theia/paytmCallback',

  //   // orderID=' + orderID + '&paytmChecksum=' + checksum + '',
  //   // 'http://localhost:5001/mymart-910ff/us-central1/helloWorld'
  //   paytmParams.body = {
  //     requestType: 'Payment',
  //     mid: MID,
  //     websiteName: 'WEBSTAGING',
  //     orderId: orderID,
  //     callbackUrl: 'http://localhost:5001/mymart-910ff/us-central1/helloWorld',

  //     txnAmount: {
  //       value: this.amount,
  //       currency: 'INR',
  //     },
  //     userInfo: {
  //       custId: custUID,
  //       // firstName: '',
  //       // lastName: '',
  //       // mobile: '',
  //       // email: ''
  //     },
  //   };

  //   const paytmChecksum = PaytmChecksum.generateSignature(JSON.stringify(paytmParams.body), MERCHANT_KEY);
  //   paytmChecksum.then((result: string) => {
  //     console.log('generateSignature Returns: ' + result);
  //     checksum = result;
  //     paytmParams.head = {
  //       channelId: 'WEB',
  //       // requestTimestamp: firebase.default.firestore.FieldValue.serverTimestamp(),
  //       signature: result
  //     };

  //     const postData = JSON.stringify(paytmParams);

  //     const options = {
  //       /* for Staging */
  //       hostname: 'securegw-stage.paytm.in',

  //       /* for Production */
  //       // hostname: 'securegw.paytm.in',

  //       port: 443,
  //       path: '/theia/api/v1/initiateTransaction?mid=' + MID + '&orderId=' + orderID + '',
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Content-Length': postData.length
  //       }
  //     };

  //     let response: string;
  //     const postReq = https.request(options, (postRes) => {
  //       postRes.on('data', (chunk) => {
  //         response += chunk;
  //       });

  //       postRes.on('end', () => {
  //         console.log('Response: ', response);
  //         response = response.substring(9);
  //         const res = JSON.parse(response);
  //         if (res.body.resultInfo.resultStatus === 'S') {
  //           console.warn(res.body.txnToken);
  //           this.token = res.body.txnToken;
  //           // this.processTransaction(orderID, MID, res.body.txnToken, MERCHANT_KEY);
  //         }
  //       });
  //     });

  //     postReq.write(postData);
  //     postReq.end();
  //   }).catch((error: any) => {
  //     console.log(error);
  //   });
  // }
}
