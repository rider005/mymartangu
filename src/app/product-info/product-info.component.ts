import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.scss'],
})
export class ProductInfoComponent implements OnInit {
  product: { [x: string]: any; name?: any; img?: any };
  constructor(
    private route: ActivatedRoute,
    private firestore: AngularFirestore,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    const catID = localStorage.getItem('catID');
    const productID = this.route.snapshot.paramMap.get('productID');

    this.firestore
      .collection('productCategories')
      .doc(catID)
      .collection('productsList')
      .doc(productID)
      .get()
      .subscribe(
        (success) => {
          if (success.exists && success.data) {
            this.product = success.data();
            console.warn(this.product);
          }
        },
        (error) => {
          console.warn(error.message);
        }
      );
    iconRegistry.addSvgIcon(
      'notLikedProduct',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/notLikedForWishList.svg'
      )
    );
  }

  ngOnInit(): void { }
}
