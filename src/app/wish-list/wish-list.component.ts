import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.scss']
})
export class WishListComponent implements OnInit {

  isWishListEmpty = true;
  constructor(firestore: AngularFirestore, auth: AngularFireAuth,) {
    this.loadWishList(auth, firestore);
  }

  ngOnInit(): void {
  }

  async loadWishList(auth: AngularFireAuth, firestore: AngularFirestore) {
    (await firestore.collection(localStorage.getItem('userWishListCollName')).ref
      .get()).docs.forEach(data => {

      })
  }

}
