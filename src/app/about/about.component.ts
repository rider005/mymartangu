import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore/';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  ColFoundN = 'foundersTeam';
  foundersList: Observable<any[]>;
  constructor(firestore: AngularFirestore) {
    this.foundersList = firestore.collection(this.ColFoundN).valueChanges();
  }

  ngOnInit(): void { }
}
