import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import firebase from 'firebase/app';
import { FooterComponent } from '../footer/footer.component';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {

  panelOpenState = true;
  feedBackFormG: FormGroup;
  userProblemFormG: FormGroup;

  userFeedback: AbstractControl;
  userProblem: AbstractControl;

  constructor(private formBuilder: FormBuilder,
    private firestore: AngularFirestore, private auth: AngularFireAuth) { }

  ngOnInit(): void {
    this.feedBackFormG = this.formBuilder.group({
      userFeedback: new FormControl('', [Validators.required,
      Validators.pattern(
        '([a-zA-Z\',.-]+( [a-zA-Z\',.-]+)*){3,300}'
      )]),
    });
    this.userProblemFormG = this.formBuilder.group({
      userProblem: new FormControl('', [Validators.required,
      Validators.pattern(
        '([a-zA-Z\',.-]+( [a-zA-Z\',.-]+)*){3,300}'
      )]),
    });
    this.userFeedback = this.feedBackFormG.get('userFeedback');
    this.userProblem = this.userProblemFormG.get('userProblem');
  }

  async onProblemFormSubmit(): Promise<void> {
    console.log('problem : ' + this.userProblemFormG.get('userProblem').value);

    try {
      (await this.firestore.collection(localStorage.getItem('allUserCollName') + '/'
        + (await this.auth.currentUser).uid
        + '/' + localStorage.getItem('userProblemsListCollName')).doc().set({
          time: firebase.firestore.FieldValue.serverTimestamp(),
          problem: this.userProblemFormG.get('userProblem').value,
        }));
      FooterComponent.openSnackBar('Problem is submitted', 'Ok');
    } catch (error) {
      console.error(error.message);
      FooterComponent.openSnackBar(error.message, 'Ok');
    }
  }

  async onFeedBackFormSubmit(): Promise<void> {
    console.log('feedBack : ' + this.feedBackFormG.get('userFeedback').value);
    try {
      (await this.firestore.collection(localStorage.getItem('allUserCollName') + '/'
        + (await this.auth.currentUser).uid
        + '/' + localStorage.getItem('userFeedBackCollName')).doc().set({
          time: firebase.firestore.FieldValue.serverTimestamp(),
          feedback: this.feedBackFormG.get('userFeedback').value,
        }));
      FooterComponent.openSnackBar('Feedback is submitted', 'Ok');
    } catch (error) {
      console.error(error.message);
      FooterComponent.openSnackBar(error.message, 'Ok');
    }
  }

}
