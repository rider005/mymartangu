import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { ProductsComponent } from './products/products.component';
import { ProductInfoComponent } from './product-info/product-info.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import {
  AngularFireAnalyticsModule,
  ScreenTrackingService,
  UserTrackingService,
} from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { SigninComponent } from './signin/signin.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
// Add the Performance Monitoring library
import 'firebase/performance';
import { AngularFirePerformanceModule, PerformanceMonitoringService } from '@angular/fire/performance';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { PaytmTransactionComponent } from './paytm-transaction/paytm-transaction.component';
import { WishListComponent } from './wish-list/wish-list.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProfileComponent,
    HomeComponent,
    CartComponent,
    ProductsComponent,
    ProductInfoComponent,
    AboutComponent,
    ContactComponent,
    SigninComponent,
    PageNotFoundComponent,
    UpdateProfileComponent,
    PaytmTransactionComponent,
    WishListComponent,
    FeedbackComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.fire),
    AngularFirestoreModule.enablePersistence({ synchronizeTabs: true }),
    AngularFirePerformanceModule,
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    AngularFireFunctionsModule,
    NgbModule,
    MaterialModule,
    AngularFireAuthModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  exports: [AngularFireModule, AngularFireAuthModule],
  providers: [ScreenTrackingService, PerformanceMonitoringService, UserTrackingService],
  bootstrap: [AppComponent],
})
export class AppModule { }
