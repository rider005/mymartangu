// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  fire: {
    apiKey: 'AIzaSyCvfFUCDBrKxrca2guGqnI42PSoB0LSb7c',
    authDomain: 'mymart-910ff.firebaseapp.com',
    databaseURL: 'https://mymart-910ff.firebaseio.com',
    projectId: 'mymart-910ff',
    storageBucket: 'mymart-910ff.appspot.com',
    messagingSenderId: '833512063510',
    appId: '1:833512063510:web:5e97a4f447646c514c2b37',
    measurementId: 'G-NX6Z26RBGN',
  },
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
