import * as functions from 'firebase-functions';
import * as nodemailer from 'nodemailer';
// before run local use this command - firebase functions:config:get > .runtimeconfig.json
// command for run emulators to test function code locally - firebase emulators:start --only functions
// for update firebase tools to latest verison - npm install -g firebase-tools@latest
// for deploy function - firebase deploy --only functions
// for deploy one 1 function - firebase deploy --only "functions:Function Name"
// npm install firebase-functions@latest --save
// npm install firebase-admin@latest --save-exact

// The Firebase Admin SDK to access Cloud Firestore.
import * as admin from 'firebase-admin';
admin.initializeApp();
import express = require('express');
import cors = require('cors');
const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
// // Add middleware to authenticate requests
// app.use(admin.auth);
app.get('/', (req, res) => {
    functions.logger.log(req.query);
    functions.logger.log(req.params);
    functions.logger.log(req.body);
    res.send('<p> Practice HTTP </p>')
});
exports.practiceHTTP = functions.https.onRequest(app);

welcomeNewUser();
// triggerNewDocument();
notifyUserWhenProfileUpdated();
deleteUser();
orderIsPlaced();
// getAllUserAndSendBirthDayWish();

console.log(sendNotificataion());
function sendNotificataion() {
    const messaging = admin.messaging();

    exports.sendNotificationToTopic = functions.database.ref('message/{topicId}')
        .onCreate(async (snapshot, context) => {
            // The topic name can be optionally prefixed with "/topics/".            
            //console.log(context.params);
            if (context.params) {
                console.log(context.params);
            }
            const topic: string = context.params.topicId;
            //console.log('Topic Name : ' + topic);            
            const topicData = snapshot.val();
            try {
                const groupName: string = await (await admin.database().ref('group/{' + topic + '}/groupInfo')
                    .once('value')).val().name;
                console.log('Name : ' + groupName);
                let message = {
                    notification: {
                        title: groupName,
                        body: '',
                    },
                    topic: topic,
                }
                if (topicData.hasChild('text')) {
                    message.notification.body = topicData.text;
                } else {
                    message.notification.body = topicData.name_doc;
                }
                const response = (await messaging.send(message));
                return 'Successfully sent message : ' + response;
            } catch (error) {
                return error.message;
            }
        });
}

// firebase functions: config: set gmail.email = "myusername@gmail.com" gmail.password = "pgjvuletrgkvmhrs"

const APP_NAME = 'My Mart';
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: gmailEmail,
        pass: gmailPassword,
    },
});

function orderIsPlaced(): void {
    exports.notifyUserWhenOrderIsPlaced = functions.firestore
        .document('allUsers/{userId}/userOrderList/{orderId}')
        .onCreate(async (snap, context) => {
            console.log('Order is created!');
            const pathToUserId = snap.ref.parent.parent?.path as string;
            return deleteCartList(snap.ref.parent.parent?.collection('userCartList').path as string, pathToUserId);
        });
}

function deleteCartList(path: string, pathToUserId: string): void {
    try {
        console.log('delete userCartList Collection Function');
        // console.log('Path : ' + path);
        // Get a new write batch
        const batch = admin.firestore().batch();
        admin.firestore()
            .collection(path).listDocuments().then(async val => {
                val.map((vall) => {
                    batch.delete(vall);
                });
                await batch.commit();
                await admin.firestore()
                    .doc(pathToUserId).update({ userCart: 0 });
            }).catch(error => {
                console.error('' + error.message);
            });
    } catch (error) {
        console.error('' + error.message);
    }
}

function notifyUserWhenProfileUpdated(): void {
    exports.notifyUserWhenProfileUpdated = functions.firestore
        .document('allUsers/{userId}')
        .onUpdate((change, context) => {
            const newValue = change.after.data();
            const oldValue = change.before.data();

            let message = '';
            let count = 0;
            if (newValue.userName !== oldValue.userName) {
                message = message.concat('Your name is updated to ' + newValue.userName + ' ');
                count = count + 1;
            }
            if (newValue.userAddress !== oldValue.userAddress) {
                message = message.concat('Your Address is updated to ' + newValue.userAddress + ' ');
                count = count + 1;
            }
            if (newValue.userCity !== oldValue.userCity) {
                message = message.concat('Your city is updated to ' + newValue.userCity + ' ');
                count = count + 1;
            }
            if (newValue.userState !== oldValue.userState) {
                message = message.concat('Your state is updated to ' + newValue.userState + ' ');
                count = count + 1;
            }
            if (count > 0) {
                message = message.trim();
                return sendProfileUpdateEmail(oldValue, newValue, message);
            } else {
                return null;
            }
        });
}

async function sendProfileUpdateEmail(
    oldValue: FirebaseFirestore.DocumentData,
    newValue: FirebaseFirestore.DocumentData, message: string): Promise<null> {
    const mailOptions = {
        from: `${APP_NAME} <noreply@firebase.com>`,
        to: oldValue.userEmail,
        subject: `Your profile is updated!`,
        // text: `Hey ${newValue.userName || ''}!, We confirm that we have deleted your ${APP_NAME} account.`,
        html: `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
        </head>
        <body align="center">
        <p>Hello ${newValue.userName}!,<br>
        Greetings of the day!,<br>
        Your below details are updated</p>
        <p>${message}</p>
        </body>
        </html>`,
    };

    // The user unsubscribed to the newsletter.
    await mailTransport.sendMail(mailOptions);
    console.log('Profile updated confirmation email sent to:', oldValue.userEmail);
    return null;
}

function welcomeNewUser(): void {
    exports.sendWelcomeEmail = functions.auth.user().onCreate(async (user) => {
        const email = user.email; // The email of the user.
        const displayName = (await admin.auth().getUser(user.uid)).displayName; // The display name of the user.        
        functions.logger.info('Account is created with email');

        // let message = {
        //     notification: {
        //         title: 'Title',
        //         body: 'Body',
        //     },
        //     topic: 'topic',
        // }
        // const response = (await admin.messaging().send(message));
        // console.log(response);

        return await sendWelcomeEmail(email, displayName);
    });
}

// Sends a welcome email to the given user.
async function sendWelcomeEmail(email: string | undefined, displayName: string | undefined): Promise<null> {
    const mailOptions = {
        from: `${APP_NAME} <noreply@firebase.com>`,
        to: email,
        subject: `Welcome to ${APP_NAME}!`,
        // text: `Hey ${displayName || ''}! Welcome to ${APP_NAME}. I hope you will enjoy our service.`,
        // html: `<html>
        // <head><title>MyMart</title></head>
        // <body>
        // <h1>Your name is ${displayName} !!!</h1>
        // <h1>Your name is ${displayName} !!!</h1>
        // </body>
        // </html>`,
        html: `
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
    <title>Email Template</title>
</head>
<body align="center">
    <img style="height: 40vh; width: 80%;"
        src="https://firebasestorage.googleapis.com/v0/b/mymart-910ff.appspot.com/o/Empty_Cart.png?alt=media&token=6f11f85c-17e0-4f2f-9e7b-afed2fb180df">
    <p></p>
    <p></p>
    <h1>${APP_NAME}</h1>
    <p></p>
    <p></p>
    <p>Welcome <b>${displayName}</b></p>
    <p>Thanks for signUp</p>
    <p></p>
    <h3>Our mission</h3>
    <p></p>
    <p>Our service is easy & safe to use.
        <br>
        Using our service you can easily order packed food
        items and more.</p>
</body>
</html>`,
    };

    await mailTransport.sendMail(mailOptions);
    console.log('New welcome email sent to:', email);
    return null;
}

function deleteUser(): void {
    exports.sendByeEmail = functions.auth.user().onDelete((user) => {
        const email = user.email; // The email of the user.
        const displayName = user.displayName; // The display name of the user.
        functions.logger.info('Account is deleted with email');
        return sendGoodbyeEmail(email, displayName);
    });
}
// Sends a goodbye email to the given user.
async function sendGoodbyeEmail(email: string | undefined, displayName: string | undefined): Promise<null> {
    const mailOptions = {
        from: `${APP_NAME} <noreply@firebase.com>`,
        to: email,
        subject: `Bye!`,
        text: `Hey ${displayName || ''}!, We confirm that we have deleted your ${APP_NAME} account.`,
    };

    // The user unsubscribed to the newsletter.
    await mailTransport.sendMail(mailOptions);
    console.log('Account deletion confirmation email sent to:', email);
    return null;
}
